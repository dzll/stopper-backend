<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->id();
            $table->integer('jenis_ID')->unsigned();
            $table->foreign('jenis_ID')->references('id')->on('jenis');
            $table->integer('supplier_ID')->unsigned();
            $table->foreign('supplier_ID')->references('id')->on('supplier');
            $table->string('kode_SPC', 15);
            $table->string('nama_SPC', 50);
            $table->string('kode_Asli', 100);
            $table->string('nama_Asli', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
