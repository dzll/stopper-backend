<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_order', function (Blueprint $table) {
            $table->id();
            $table->integer('repeat_order_ID')->unsigned();
            $table->foreign('repeat_order_ID')->references('id')->on('repeat_order');
            $table->integer('stok_ID')->unsigned();
            $table->foreign('stok_ID')->references('id')->on('stok');
            $table->integer('qty');
            $table->integer('harga');
            $table->integer('total');
            $table->integer('status_PO');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_order');
    }
}
