<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stok', function (Blueprint $table) {
            $table->id();
            $table->integer('detail_item_ID')->unsigned();
            $table->foreign('detail_item_ID')->references('id')->on('detail_item');
            $table->integer('kartu_stok_ID')->unsigned();
            $table->foreign('kartu_stok_ID')->references('id')->on('kartu_stok');
            $table->integer('lokasi_ID')->unsigned();
            $table->foreign('lokasi_ID')->references('id')->on('lokasi');
            $table->integer('jumlah_stok');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stok');
    }
}
