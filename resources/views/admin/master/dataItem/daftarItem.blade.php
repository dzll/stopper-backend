@extends('layout.app')

@push('css')
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Master</h1>
          </div><!-- /.col -->
          {{-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div> --}}
        </div>
      </div>
    </div>
    <section class="content">
      <div class="container-fluid">
        <div>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Item</h3>
              <div class="card-tools">
                  <a href="#" class="btn btn-sm btn-primary float-right mt-1 mb-1" type="button" data-toggle="modal" data-target="#modal-tambah-item"><i class="fa fa-plus"></i> &nbsp;Tambah Item</a>
              </div>
            </div>
            <div class="card-body">
              <ul class="nav nav-tabs mb-3" id="custom-content-above-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Parfum</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-content-above-profile-tab" data-toggle="pill" href="#custom-content-above-profile" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Botol</a>
                </li>
              </ul>
              <!-- <div class="tab-custom-content">
                <p class="lead mb-0">Custom Content goes here</p>
              </div> -->
              <div class="tab-content" id="custom-content-above-tabContent">
                <div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                  <table id="tableItem" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="3%">NO</th>
                        <th width="7%">Kode Item</th>
                        <th width="20%">Nama Item</th>
                        <th width="20%">Suplier</th>
                        <th width="20%">Harga</th>
                        <th width="20%">Satuan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>SP879244</td>
                        <td>Win 95+</td>
                        <td>Pt Satu</td>
                        <td>Rp 50000</td>
                        <td>Ml</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>SP879189</td>
                        <td>Win 95+</td>
                        <td>Pt Satu</td>
                        <td>Rp 40000</td>
                        <td>Ml</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>SP879231</td>
                        <td>Win XP SP2+</td>
                        <td>Pt Satu</td>
                        <td>Rp 20000</td>
                        <td>Ml</td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>SP879896</td>
                        <td>Win XP</td>
                        <td>Pt Empat</td>
                        <td>Rp 45000</td>
                        <td>Ml</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane fade" id="custom-content-above-profile" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
                  <table id="tableItem2" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Rendering engine</th>
                      <th>Browser</th>
                      <th>Platform(s)</th>
                      <th>Engine version</th>
                      <th>CSS grade</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>Trident</td>
                      <td>Internet
                        Explorer 4.0
                      </td>
                      <td>Win 95+</td>
                      <td> 4</td>
                      <td>X</td>
                    </tr>
                    <tr>
                      <td>Gecko</td>
                      <td>Firefox 1.0</td>
                      <td>Win 98+ / OSX.2+</td>
                      <td>1.7</td>
                      <td>A</td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="modal-tambah-item">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <!-- <form role="form" action="" method="POST"> -->
                  <!-- @csrf -->
                  <div class="modal-header">
                    <h4 class="modal-title">Tambah Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body"> 
                    <div class="form-group">
                      <label>Nama Item</label>
                      <input class="form-control" id="nama_item" name="nama_item" type="nama_item" required/>
                    </div>
                    <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label for="suplier" class="">Suplier</label>
                            <div class="input-group">
                              <select class="form-control" name="suplier" id="suplier" required>
                                <option disabled selected>- Pilih Suplier  -</option>
                                <option>Supplier 1</option>
                                <option>Supplier 2</option>
                                <option>Supplier 3</option>
                              </select>
                              <div class="input-group-prepend ml-2">
                                <span><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="jenis" class="">Jenis</label>
                            <select class="form-control" name="jenis" id="jenis" required>
                                <option disabled selected>- Pilih Jenis  -</option>
                            </select>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <label for="harga_jenis" class="">Nama ukuran</label>
                            <select class="form-control" name="harga_jenis" id="harga_jenis" required>
                                <option disabled selected>- Pilih nama ukuran  -</option>
                            </select>
                          </div>
                          <div class="col-md-6">
                            <label for="satuan" class="">Satuan</label>
                            <select class="form-control" name="satuan" id="satuan" required>
                                <option disabled selected>- Pilih Satuan  -</option>
                            </select>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> &nbsp;Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> &nbsp;Tambah</button>
                  </div>
                <!-- </form> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
  

@push('js')
  <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
  <script>
    $(function() {
      $("#tableItem").DataTable({
        paging: true,
        lengthChange: true,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
        responsive: true
      });

      $("#tableItem2").DataTable({
        paging: true,
        lengthChange: true,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
        responsive: true
      });
    });
  </script>
@endpush