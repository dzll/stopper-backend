<!doctype html>
<html lang="en">
  <head>
  	<title>Stopper | Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
    <style>
        .z-index-set {
            z-index: -10;
        }
        .btn-z-index {
            z-index: 10;
        }
    </style>
	</head>
	<body>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
                    <img src="https://www.stopper.co.id/wp-content/uploads/2015/09/grosir-parfum-refill-online.png" alt="Stopper Logo" height="70vw" class="brand-text mx-auto d-block">
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-5">
					<div class="login-wrap pt-5 pl-5 pr-5" style="padding-bottom: 7rem">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-user-o"></span>
                        </div>
                        <h3 class="text-center mb-4">Welcome Back</h3>
                        <form action="#" class="login-form">
                            <div class="form-group">
                                <input type="text" class="form-control rounded-left" placeholder="Username" required>
                            </div>
                            <div class="form-group d-flex">
                                <input type="password" class="form-control rounded-left" placeholder="Password" required>
                            </div>
                            <div class="form-group d-md-flex">
                                <div class="w-0 text-md-right">
                                    <a href="#">Forgot Password</a>
                                </div>
                            </div>
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary rounded submit p-3 px-5 btn-z-index"><b>Login</b></button>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
		</div>
	</section>

	{{-- <script src="js/jquery.min.js"></script> --}}
    <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/popper.jss')}}"></script>
    {{-- <script src="js/bootstrap.min.js"></script> --}}
    <script src="{{asset('js/main.jss')}}"></script>

	</body>
</html>