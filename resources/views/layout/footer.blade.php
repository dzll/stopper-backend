<footer class="main-footer">
    <div class="float-right d-sm-none d-md-block">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2022 <a href="http://adminlte.io">STOPPER@DEV</a>.</strong> All rights reserved.
</footer>