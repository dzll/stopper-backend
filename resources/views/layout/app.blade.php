<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>STOPPER | DEV</title>
        {{-- SIDEBAR --}}
        <style>
            .style-3::-webkit-scrollbar 
            {
            width: 6px;
            background-color: #212529;
            }
            
            .style-3::-webkit-scrollbar-thumb 
            {
            background-color: gray;
            }
        </style>
        {{-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> --}}
        {{-- <link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}"> --}}
        <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/toastr/toastr.min.css')}}">
        {{-- <link rel="stylesheet" href="{{asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}"> --}}
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        @stack('css')
        
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
        
            @include('layout.header')
            @include('layout.sidebar')
            @yield('content')
            @include('layout.footer')

            {{-- <aside class="control-sidebar control-sidebar-dark">
            </aside> --}}

        </div>
        <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
        {{-- <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script> --}}
        <script>
          $.widget.bridge('uibutton', $.ui.button)
        </script>
        <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('assets/dist/js/adminlte.js')}}"></script>
        <script src="{{asset('assets/dist/js/demo.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
        <script src="{{asset('assets/plugins/toastr/toastr.min.js')}}"></script>
        {{-- <script src="{{asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script> --}}
        <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
        
        @include('include.notify-messages')
        @stack('js')

    </body>
</html>
