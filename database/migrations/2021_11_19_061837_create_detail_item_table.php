<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_item', function (Blueprint $table) {
            $table->id();
            $table->integer('packing_ID')->unsigned();
            $table->foreign('packing_ID')->references('id')->on('packing');
            $table->integer('item_ID')->unsigned();
            $table->foreign('item_ID')->references('id')->on('item');
            $table->integer('harga_jual');
            $table->integer('harga_beli');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_item');
    }
}
