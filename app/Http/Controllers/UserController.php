<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class UserController extends Controller
{
    //
    public function login(Request $request, User $user)
    {
        if(Auth::attempt(['username'=>$request->username, 'password'=>$request->password]))
        {
            $users = $user->find(Auth::user()->id);
            return response()->json(['status'=>200,'message'=>'OK','id'=>$users->id,'username'=>$users->username]);
        }else {
            return response()->json(['status'=>401,'message'=>'Unauthorized']);
        }   
    }
}
